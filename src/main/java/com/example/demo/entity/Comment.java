package com.example.demo.entity;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name = "comments")
public class Comment {
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String text;
	@Column(name = "report_id")
	private int reportId;
	@Column(name = "created_date")
	private Timestamp createdDate;
	@Column(name = "updated_date")
	private Timestamp updatedDate;

	public String getStrCreatedDate() {
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(createdDate);
		return date;
	}

	public String getStrUpdatedDate() {
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(updatedDate);
		return date;
	}

	@PrePersist
	public void onPrePersist() {
		setCreatedDate(new Timestamp(System.currentTimeMillis()));
		setUpdatedDate(new Timestamp(System.currentTimeMillis()));
	}

	@PreUpdate
	public void onPreUpdate() {
		setUpdatedDate(new Timestamp(System.currentTimeMillis()));
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getReportId() {
		return reportId;
	}

	public void setReportId(int reportId) {
		this.reportId = reportId;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public Timestamp getUpdatedDate() {
		return updatedDate;
	}
}
