package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class CommentController {

	@Autowired
	CommentService commentService;
	@Autowired
	ReportService reportService;

	// コメント投稿機能
	@PostMapping("/comment/add")
	public ModelAndView addComment(@ModelAttribute("commentFormModel")Comment comment ) {
		commentService.saveComment(comment);
		Report report = reportService.editReport(comment.getReportId());
		report.setUpdatedDate(comment.getUpdatedDate());
		reportService.saveReport(report);
		return new ModelAndView("redirect:/");
	}

	// コメント削除機能
	@DeleteMapping("comment/delete/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}

	// 編集画面
	@GetMapping("comment/edit/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Comment comment = commentService.editComment(id);
		mav.addObject("commentFormModel", comment);
		mav.setViewName("comment-edit");
		return mav;
	}

	// 編集処理
	@PutMapping("/comment/update/{id}")
	public ModelAndView updateComment(@PathVariable Integer id, @ModelAttribute("commentFormModel") Comment comment) {
		comment.setId(id);
		commentService.saveComment(comment);
		Report report = reportService.editReport(comment.getReportId());
		report.setUpdatedDate(comment.getUpdatedDate());
		reportService.saveReport(report);
		return new ModelAndView("redirect:/");
	}
}
