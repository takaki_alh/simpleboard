package com.example.demo.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.DateSerch;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {

	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表示画面
	@GetMapping
	public ModelAndView top(@ModelAttribute("serchForm") DateSerch dateSerch) {
		Timestamp start = formatChange(dateSerch.getStart(), "start");
		Timestamp end = formatChange(dateSerch.getEnd(), "end");
		ModelAndView mav = new ModelAndView();

		// 投稿を全件取得
		List<Report> contentData = reportService.findAllReport(start, end);
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);

		// 新規投稿フォーム
		Report report = new Report();
		mav.addObject("formModel", report);

		// 返信フォーム
		Comment comment = new Comment();
		mav.addObject("commentFormModel", comment);

		// 返信を全件取得
		List<Comment> commentDate = commentService.findAllComment();
		// 返信オブジェクトを保管
		mav.addObject("comments", commentDate);

		dateSerch = new DateSerch();
		mav.addObject("serchFormModel", dateSerch);

		return mav;
	}

	public Timestamp formatChange(String date, String check) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Timestamp ts = null;

		if (date == null || date == "") {
			if (check.equals("start")) {
				date = "2021-01-01 00:00:00";
			} else {
				ts = new Timestamp(System.currentTimeMillis());
				int plusOneMinuts = ts.getMinutes() + 1;
				ts.setMinutes(plusOneMinuts);
				return ts;
			}
		} else {
			if (check.equals("start")) {
				date = date + " 00:00:00";
			} else {
				date = date + " 23:59:59";
			}
		}

		Date parseDate = null;
		try {
			parseDate = format.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		ts = new Timestamp(parseDate.getTime());

		return ts;

	}

	// 編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集する投稿を取得
		Report report = reportService.editReport(id);
		// 編集する投稿をセット
		mav.addObject("formModel", report);
		// 画面遷移先を指定
		mav.setViewName("/edit");
		return mav;
	}

	// 編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		// UrlParameterのidを更新するentityにセット
		report.setId(id);
		// 編集した投稿を更新
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		reportService.deleteReport(id);
		return new ModelAndView("redirect:/");
	}

}
