package com.example.demo.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {

	@Autowired
	ReportRepository reportRepository;


	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// レコード削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	// レコード１件取得
	public Report editReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

	// レコード全件取得（更新日ソート）
	public List<Report> findAllReport(Timestamp start, Timestamp end) {
		return reportRepository.findAll(start, end);
	}
}
