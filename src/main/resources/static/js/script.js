/**
 *
 */
$(function() {
	let cookie = $.cookie('open');
	let contentIdCookie = $.cookie('contentId');
	let open = false;

	if ($.cookie('open') == 'on') {
		$('.comments').css('display', 'block');
		$.cookie('open', 'on');
		open = true;
	}



	$('.test', function() {
		$(this).find('.comment-form-open').find('.open').on("click",
				function() {

			contentId = $(this).parent().find('.content-id').text();
			console.log(contentId);

					if (open) {
						$(this).parent().find('.comments').slideUp('slow');
						open = false;

					} else {
						$(this).parent().find('.comments').slideDown('slow');
						open = true;
					}
				})

	})

	$('#hold').click(function() {

		if ($.cookie('open') == 'on') {
			$('.comments').css('display', 'none');
			$.cookie('open', 'off');
			open = false;
		} else {
			$('.comments').css('display', 'block');
			$.cookie('open', 'on');
			open = true;
		}

	})

})